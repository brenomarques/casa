package br.com.casa.server.api;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.casa.server.RobotModel;
import br.com.casa.shared.CardinalPoints;
import br.com.casa.shared.Robot;
import br.com.casa.shared.exceptions.RobotException;
/*
This API provides information about the robots and also allows you to send command to modify the position
and direction of the robots.

Parameter: command
Commands: L - makes the robot rotate 90 degrees to the left;
          R - makes the robot rotate 90 degrees to the right;
          M - makes the robot walk a forward position;
          N - makes the robot rotate fully to the north;
          S - makes the robot rotate fully to the South;
          W - makes the robot rotate fully to the West;
          E - makes the robot rotate fully to the East;
Note: Activate the API without parameters to receive the position of robots.

Returns: JSON with the position of the robots.
Samples:
{"Robot": {"pointX": - 5, "pointy": 5, "cardinalPoint", "W"}}.

Errors:
RobotExceptionCardinalPoint;
RobotExceptionPositionX;
RobotExceptionPositionY;
RobotExceptionRotate.
Samples:
{"Error": {"typeException": "br.com.casa.shared.exceptions.RobotExceptionPositionX: Invalid PositionX."}, "Robot": {"pointX": - 5, "pointy": 5, "cardinalPoint" "W"}}

Samples to make the robot walk two positions forward, turn 90 degrees and walk two more positions forward:
/ Backend / apiV1? Command = MMLMM

Samples to make the robot turn to the north:
/ Backend / apiV1? Command = N
*/
public class ApiV1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PARAM_COMMAND = "command";
	private static final String COMMAND_MOVEMENT = "M";
	private static final String TYPE_EXCEPTION = "typeException";
	private static final String ROBOT = "robot";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String result = "";
		String command = req.getParameter(PARAM_COMMAND);
		command = (command == null )? "": command.toUpperCase();
			try {
				result = translateCommand(command).toString();
			} catch (Exception e) {
				result = Robot.MESSAGE_ERROR_JSON;
				e.printStackTrace();
			}
		resp.setCharacterEncoding("UTF-8");
		PrintWriter writer = resp.getWriter();
		writer.write(result);
		writer.flush();
	}

	private JSONObject translateCommand(String command) throws JSONException {
		JSONObject jsonObjectResult = new JSONObject();
		RobotModel robotModel = RobotModel.get();
		try {
			for (int index = 0; index < command.length(); index++) {
				String commandItem = command.substring(index, index + 1);
				if ("LR".contains(commandItem)) {
					commandItem = CardinalPoints.getCardinalPointRotate90(robotModel.getCardinalPoint(), commandItem);
				}

				if (CardinalPoints.isCardinalPoint(commandItem)) {
					robotModel.setCardinalPoint(commandItem);
				} else if (commandItem.equals(COMMAND_MOVEMENT)) {
					if (robotModel.getCardinalPoint().equals(CardinalPoints.WEST)) {
						robotModel.setPointX(robotModel.getPointX() - 1);
					} else if (robotModel.getCardinalPoint().equals(CardinalPoints.EAST)) {
						robotModel.setPointX(robotModel.getPointX() + 1);
					} else if (robotModel.getCardinalPoint().equals(CardinalPoints.NORTH)) {
						robotModel.setPointY(robotModel.getPointY() + 1);
					} else if (robotModel.getCardinalPoint().equals(CardinalPoints.SOUTH)) {
						robotModel.setPointY(robotModel.getPointY() - 1);
					}
				}
				RobotModel.set(robotModel.getRobot());
			}
			RobotModel.set(robotModel.getRobot());
		} catch (RobotException e) {
			JSONObject jsonObjectErro = new JSONObject();
			jsonObjectErro.put(TYPE_EXCEPTION, e);
			jsonObjectResult.put("error", jsonObjectErro);
			e.printStackTrace();
		}
		jsonObjectResult.put(ROBOT, RobotModel.get().getJSONObject());
		return jsonObjectResult;

	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doPost(req, resp);
		doGet(req, resp);
	}

}
