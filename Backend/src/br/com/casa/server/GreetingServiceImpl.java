package br.com.casa.server;

import br.com.casa.client.GreetingService;
import br.com.casa.shared.Robot;
import br.com.casa.shared.exceptions.RobotExceptionCardinalPoint;
import br.com.casa.shared.exceptions.RobotExceptionPositionX;
import br.com.casa.shared.exceptions.RobotExceptionPositionY;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
		GreetingService {
	

	public Robot getRobot()  {
	  return RobotModel.get().getRobot();
	}
	
	public Robot setRobot(Robot robot) throws RobotExceptionCardinalPoint, RobotExceptionPositionX, RobotExceptionPositionY
	{
		 RobotModel.set(robot);
		 return RobotModel.get().getRobot();
	}
	
	
}

