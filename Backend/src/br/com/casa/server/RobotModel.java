package br.com.casa.server;

import javax.jdo.PersistenceManager;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import br.com.casa.shared.CardinalPoints;
import br.com.casa.shared.Robot;
import br.com.casa.shared.exceptions.RobotExceptionCardinalPoint;
import br.com.casa.shared.exceptions.RobotExceptionPositionX;
import br.com.casa.shared.exceptions.RobotExceptionPositionY;

/*
 * Implementing all of the robots data storage logic.
 */
@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class RobotModel {
	public static Key robotModelKey = KeyFactory.createKey(RobotModel.class.getSimpleName(), "robot01@casa.com.br");
	private static final String CARDINAL_POINT = "cardinalPoint";
	private static final String POINT_X = "pointX";
	private static final String POINT_Y = "pointY";

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key  key;
	@Persistent
	private String cardinalPoint;
	@Persistent
	private int pointX;
	@Persistent
	private int pointY;
	
	public RobotModel(){
		this.cardinalPoint = CardinalPoints.NORTH;
		this.pointX = CardinalPoints.MIN_X;
		this.pointY = CardinalPoints.MIN_Y;
	}
	
    public void setKey(Key key) {
        this.key = key;
    }
	
	public boolean isPointX(int pointX){
		return ( ( pointX >= CardinalPoints.MIN_X) && ( pointX <= CardinalPoints.MAX_X));
	}

	
	public boolean isPointY(int pointY){
		return ( ( pointY >= CardinalPoints.MIN_Y) && ( pointY <= CardinalPoints.MAX_Y));
	}
	
	public String getCardinalPoint() {
		return cardinalPoint;
	}
	public void setCardinalPoint(String cardinalPoint) throws RobotExceptionCardinalPoint {
		if ( !CardinalPoints.isCardinalPoint(cardinalPoint))
			throw new RobotExceptionCardinalPoint();
		this.cardinalPoint = cardinalPoint;
	}
	public int getPointX() {
		return pointX;
	}
	public void setPointX(int pointX) throws RobotExceptionPositionX {
		if ( !isPointX(pointX))
			throw new RobotExceptionPositionX();
	    this.pointX = pointX;
	}
	public int getPointY() {
		return pointY;
	}
	public void setPointY(int pointY) throws RobotExceptionPositionY {
		if ( !isPointY(pointY))
			throw new RobotExceptionPositionY(); 
		this.pointY = pointY;
		
	}
	
	public Robot getRobot(){
		return new Robot(getCardinalPoint(), getPointX(), getPointY());
	}
	
	
    public static RobotModel get(){
    	PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			RobotModel robotModel;
			try {
			    robotModel = (RobotModel) pm.getObjectById(RobotModel.class, robotModelKey);
			} catch (javax.jdo.JDOObjectNotFoundException notFound) {
				robotModel = new RobotModel();
				robotModel.setKey(robotModelKey);
				pm.makePersistent(robotModel);
		
			}
			return robotModel;
		}
		finally {
			pm.close();
		}
    }
    
    public static void set(Robot robot) throws RobotExceptionCardinalPoint, RobotExceptionPositionX, RobotExceptionPositionY{
    	PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			RobotModel robotModel = get();
			robotModel.setCardinalPoint(robot.getCardinalPoint());
			robotModel.setPointX(robot.getPointX());
			robotModel.setPointY(robot.getPointY());
			pm.makePersistent(robotModel);
		}
		finally {
			pm.close();
		}
    }
    
    public JSONObject getJSONObject() throws JSONException{
    	JSONObject  jsonObject = new JSONObject();
    	jsonObject.put(CARDINAL_POINT, getCardinalPoint());
    	jsonObject.put(POINT_X, getPointX());
    	jsonObject.put(POINT_Y, getPointY());
    	return jsonObject;
   }
    
   
	
}
