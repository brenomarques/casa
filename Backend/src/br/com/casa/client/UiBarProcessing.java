package br.com.casa.client;

import br.com.casa.resources.AppImages;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;

public class UiBarProcessing {

	private static UiBarProcessing uiBarProcessing;
	DialogBox dialogBox;

	public UiBarProcessing() {

		dialogBox = new DialogBox();
		dialogBox.setGlassEnabled(true);
		//dialogBox.setAnimationEnabled(true);
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		Image imageProcessing = new Image(AppImages.INSTANCE.uiBarProcessing());
		Label labelProcessing = new Label("Processando...");
		horizontalPanel.setSpacing(10);
		horizontalPanel.add(imageProcessing);
		horizontalPanel.add(labelProcessing);
		dialogBox.add(horizontalPanel);
	}

	private static UiBarProcessing getUiBarProcessing() {
		if (uiBarProcessing == null)
			uiBarProcessing = new UiBarProcessing();
		return uiBarProcessing;
	}

	public static void beginProcessing() {
		if (getUiBarProcessing().dialogBox.isShowing() == false) {
			int left = Window.getClientWidth()/ 2;
            int top = Window.getClientHeight()/ 2;
            getUiBarProcessing().dialogBox.setPopupPosition(left - 100, top- 150);
			getUiBarProcessing().dialogBox.show();
			
		}

	}

	public static void endProcessing() {
		getUiBarProcessing().dialogBox.hide();

	}

}
