package br.com.casa.client;

import br.com.casa.shared.Robot;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	void getRobot(AsyncCallback<Robot> callback);
	void setRobot(Robot robot, AsyncCallback<Robot> callback);
}
