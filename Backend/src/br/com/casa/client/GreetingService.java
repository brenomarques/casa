package br.com.casa.client;

import br.com.casa.shared.Robot;
import br.com.casa.shared.exceptions.RobotExceptionCardinalPoint;
import br.com.casa.shared.exceptions.RobotExceptionPositionX;
import br.com.casa.shared.exceptions.RobotExceptionPositionY;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	Robot getRobot() ;
	Robot setRobot(Robot robot) throws  RobotExceptionCardinalPoint, RobotExceptionPositionX, RobotExceptionPositionY ;
}
