package br.com.casa.client;

import br.com.casa.resources.AppImages;
import br.com.casa.shared.CardinalPoints;
import br.com.casa.shared.Robot;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;


/**
 * Monitoring robots, also allows you to modify the position and direction of the robots..
 */
public class Backend implements EntryPoint, ClickHandler, ChangeHandler {
	
	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	private int PIXEL_SCALE = 90;

	
	private AbsolutePanel rectanglePanel;
	private Image imageRobot;
	private TextBox  textBoxX;
	private TextBox  textBoxY;
	private ListBox listBoxCardinalPoints;
	private Button buttonUpdate;
	private Button buttonRecover;
	private Robot robot;
	
	
	
	public void onModuleLoad() {
		rectanglePanel = new AbsolutePanel();
		textBoxX = new TextBox();
		textBoxY = new TextBox();
		imageRobot = new Image();
		listBoxCardinalPoints = new ListBox();
		buttonUpdate = new Button("Update");
		buttonUpdate.addClickHandler(this);
		buttonRecover = new Button("Recover position");
		buttonRecover.setWidth("150px");
		buttonRecover.addClickHandler(this);
		for( String cardinalPoints: CardinalPoints.LIST_CARDINAL_POINTS){
			listBoxCardinalPoints.addItem(getTitle(cardinalPoints), cardinalPoints);
		}
		listBoxCardinalPoints.addChangeHandler(this);
		RootPanel.get("rectangle_id").add(rectanglePanel);
		RootPanel.get("text_box_x_id").add(textBoxX);
		RootPanel.get("text_box_y_id").add(textBoxY);
		RootPanel.get("list_box_cardinal_points_id").add(listBoxCardinalPoints);
		RootPanel.get("button_update_id").add(buttonUpdate);
		RootPanel.get("button_recover_id").add(buttonRecover);
		rectanglePanel.setSize((CardinalPoints.MAX_X * PIXEL_SCALE) + "px", (CardinalPoints.MAX_Y * PIXEL_SCALE) + "px");
		rectanglePanel.add(imageRobot, 10, 100);
		recoverRobotPosition();
		
	    KeyUpHandler repositionHandler = new KeyUpHandler() {
	      public void onKeyUp(KeyUpEvent event) {
	        checkNeedUpdate();
	      }
	    };
	    textBoxX.addKeyUpHandler(repositionHandler);
	    textBoxY.addKeyUpHandler(repositionHandler);
		
	}


	@Override
	public void onClick(ClickEvent event) {
		if ( buttonUpdate.equals(event.getSource()) ){
			UpdateRobotPosition();
		} else if (buttonRecover.equals(event.getSource())){
			recoverRobotPosition();
		}
		
	}
	
	@Override
	public void onChange(ChangeEvent event) {
		if ( listBoxCardinalPoints.equals(event.getSource())){
			checkNeedUpdate();
		}
	}
	
	private Robot getRobot() throws ExceptionRobotNotAssigned{
		if (this.robot == null)
			throw new ExceptionRobotNotAssigned("Robot is not assigned");
		
	     return this.robot; 
	}
	
	
	private void setRobot(Robot robot) {
	    this.robot = robot; 
	    updateRobotStatus();
	}
	
    private void recoverRobotPosition() {
    	UiBarProcessing.beginProcessing();
		greetingService.getRobot(new AsyncCallback<Robot>() {
			@Override
			public void onSuccess(Robot robot) {
				setRobot(robot);
				updateRobotStatus();
				UiBarProcessing.endProcessing();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
				UiBarProcessing.endProcessing();
				
			}
		});
		
				
	}
    
	private void UpdateRobotPosition() {
		Integer pointX = getInt(textBoxX.getText(), "Invalid PositionX.");
		Integer pointY = getInt(textBoxY.getText(), "Invalid PositionY.");
		if ((pointX != null) && (pointY != null)) {
			
			int selectedCardinalPoint = listBoxCardinalPoints
					.getSelectedIndex();
			String cardinalPoint = listBoxCardinalPoints
					.getValue(selectedCardinalPoint);
			try {
				Robot robot = getRobot();
				robot.setCardinalPoint(cardinalPoint);
				robot.setPointX(pointX);
				robot.setPointY(pointY);
				UiBarProcessing.beginProcessing();
				greetingService.setRobot(robot, new AsyncCallback<Robot>() {
					@Override
					public void onSuccess(Robot robotUpdated) {
						setRobot(robotUpdated);
						UiBarProcessing.endProcessing();
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.getMessage());
						UiBarProcessing.endProcessing();
					}
				});

			} catch (ExceptionRobotNotAssigned e) {
				Window.alert(e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	private void updateRobotStatus(){
		try {
			Robot robot = getRobot();
			updateRobotCardinalPoint(robot.getCardinalPoint());
			textBoxX.setText(String.valueOf(robot.getPointX()));
			textBoxY.setText(String.valueOf(robot.getPointY()));
			int cardinalPointSelected = CardinalPoints.getCardinalPointIndex(robot.getCardinalPoint());
			listBoxCardinalPoints.setSelectedIndex(cardinalPointSelected);
			rectanglePanel.setWidgetPosition(imageRobot, getPointLeft(robot), getPointTop(robot));
			checkNeedUpdate();
		} catch (ExceptionRobotNotAssigned e) {
			Window.alert(e.getMessage());
			e.printStackTrace();
		}
	}
	
	private String getTitle(String cardinalPoint ){
		switch (cardinalPoint){
           case CardinalPoints.NORTH : return "NORTH";
           case CardinalPoints.SOUTH : return "SOUTH";
           case CardinalPoints.EAST : return "EAST";
           case CardinalPoints.WEST : return "WEST";
           default : return "N/A";
     	}
	}
	
	private void updateRobotCardinalPoint(String cardinalPoint){
		switch (cardinalPoint){
        case( CardinalPoints.NORTH) : 
        	imageRobot.setResource(AppImages.INSTANCE.robot_north());
            break;  
        case( CardinalPoints.SOUTH ) : 
        	imageRobot.setResource(AppImages.INSTANCE.robot_south());
            break;  
        case( CardinalPoints.EAST ) : 
        	imageRobot.setResource(AppImages.INSTANCE.robot_east());
            break;  
        case( CardinalPoints.WEST) : 
        	imageRobot.setResource(AppImages.INSTANCE.robot_west());
            break;  
  	  }
	}
	
	protected void checkNeedUpdate() {
		try {
			Robot robot = getRobot();
		    int pointX = getInt(textBoxX.getText());
		    int pointY = getInt(textBoxY.getText());
		    int selectedCardinalPoint = listBoxCardinalPoints.getSelectedIndex();
			String cardinalPoint  = listBoxCardinalPoints.getValue(selectedCardinalPoint);
			Boolean needUpdate = ((robot.getPointX() != pointX) || (robot.getPointY() != pointY) || !robot.getCardinalPoint().equals(cardinalPoint));
			buttonUpdate.setVisible(needUpdate);
			
		} catch (ExceptionRobotNotAssigned e) {
			Window.alert(e.getMessage());
			e.printStackTrace();
		}
		
		
	}
	
	private class ExceptionRobotNotAssigned extends Exception{
		private static final long serialVersionUID = 1L;

		public ExceptionRobotNotAssigned(String message) {
			super(message);
		}
		
	}
	
	private int getPointLeft(Robot robot){
		if ( robot.getPointX() == 0  )
			return (rectanglePanel.getOffsetHeight() / 2) - 40; 
		if ( robot.getPointX() > 0 )
			return  (( (CardinalPoints.MAX_X + robot.getPointX() )  * ( imageRobot.getOffsetWidth() - 20 ) )) / 2 ;
		else
			return  (( (CardinalPoints.MAX_X + robot.getPointX() )  * (imageRobot.getOffsetWidth() - 15))) / 2 ;
	}
	
	private int getPointTop(Robot robot){
		if ( robot.getPointY() == 0  )
			return (rectanglePanel.getOffsetHeight() / 2) - 45; 
		if ( robot.getPointY() > 0 )
		   return  (( (CardinalPoints.MAX_Y - robot.getPointY() )  * imageRobot.getOffsetWidth())) / 2 ;
		else 
		   return  (( (CardinalPoints.MAX_Y - robot.getPointY() )  * (imageRobot.getOffsetWidth() - 15))) / 2 ;
	
	}
	
	private int getInt(String text) {
		try {
			return Integer.valueOf(text);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	private Integer getInt(String text, String errorMessage ) {
		try {
			return Integer.valueOf(text);
		} catch (Exception e) {
			Window.alert(errorMessage);
			e.printStackTrace();
			return null;
		}
	}
	
	
}
