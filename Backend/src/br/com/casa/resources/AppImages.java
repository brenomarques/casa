package br.com.casa.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface AppImages extends ClientBundle {
	public static final AppImages INSTANCE = GWT.create(AppImages.class);
	ImageResource uiBarProcessing();
	ImageResource robot_east();
	ImageResource robot_north();
	ImageResource robot_south();
	ImageResource robot_west();
}
