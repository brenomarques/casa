package br.com.casa.shared.exceptions;

import br.com.casa.shared.Robot;


public class RobotException extends Exception {
	
	private static final long serialVersionUID = 1L;
	public RobotException(){
		super(Robot.MESSAGE_ERROR_JSON);
	}
	public RobotException(String string) {
		super(string);
	}
	

}
