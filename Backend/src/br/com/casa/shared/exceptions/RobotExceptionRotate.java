package br.com.casa.shared.exceptions;


public class RobotExceptionRotate extends RobotException {
	
	private static final long serialVersionUID = 1L;
	
	public RobotExceptionRotate(){
		super("Invalid robot rotate.");
	}

}
