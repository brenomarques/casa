package br.com.casa.shared.exceptions;

public class RobotExceptionPositionY extends RobotException {
	private static final long serialVersionUID = 1L;
	
	public RobotExceptionPositionY(){
		super("Invalid PositionY.");
	}

}
