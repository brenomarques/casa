package br.com.casa.shared.exceptions;

public class RobotExceptionPositionX extends RobotException {
	private static final long serialVersionUID = 1L;

	public RobotExceptionPositionX(){
		super("Invalid PositionX.");
	}
}
