package br.com.casa.shared.exceptions;



public class RobotExceptionCardinalPoint extends RobotException {
	private static final long serialVersionUID = 1L;
	
	public RobotExceptionCardinalPoint(){
		super("Invalid cardinal point.");
	}

}
