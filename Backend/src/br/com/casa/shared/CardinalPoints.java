package br.com.casa.shared;

import br.com.casa.shared.exceptions.RobotExceptionRotate;

/*
 * Helper functions for cardinal points
 */
public class CardinalPoints {
	public final static String NORTH = "N";
	public final static String SOUTH = "S";
	public final static String EAST  = "E";
	public final static String WEST  = "W";
	public final static String ROTATE_90_LEFT  = "L";
	public final static String ROTATE_90_RIGHT  = "R";
	
	public final static int MIN_X = -5;
	public final static int MIN_Y = -5;
	public final static int MAX_X = 5;
	public final static int MAX_Y = 5;
	public final static String[] LIST_CARDINAL_POINTS = new String[]{NORTH,EAST,SOUTH,WEST};
	
	public static int getCardinalPointIndex(String cardinalPoint){
		int index = 0;
		for( String cardinalPointItem: CardinalPoints.LIST_CARDINAL_POINTS){
			if ( cardinalPointItem.equals(cardinalPoint))
				break;
			else 
			   index++;
		}
		return index;
	}
	
	public static Boolean isCardinalPoint(String cardinalPoint){
		for( String cardinalPointItem: CardinalPoints.LIST_CARDINAL_POINTS){
			if ( cardinalPointItem.equals(cardinalPoint))
				return true;
		}
		return false;
	}
	
	public static String getCardinalPointRotate90(String cardinalPoint, String rotate90) throws RobotExceptionRotate{
		int cardinalPointIndex = getCardinalPointIndex(cardinalPoint) ;
		int newCardinalPointIndex = 0 ; 
		if ( ROTATE_90_LEFT.equals(rotate90))
			newCardinalPointIndex = cardinalPointIndex -1;
		else if ( ROTATE_90_RIGHT.equals(rotate90))
			newCardinalPointIndex = cardinalPointIndex +1;
		else 
			throw new RobotExceptionRotate();
			
		if ( newCardinalPointIndex < 0 )
				return WEST;
		else if ( newCardinalPointIndex >= LIST_CARDINAL_POINTS.length )
			return NORTH;
		else 
			return LIST_CARDINAL_POINTS[newCardinalPointIndex];
	}
	
	
	
	
	
}
