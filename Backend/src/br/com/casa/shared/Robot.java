package br.com.casa.shared;

import java.io.Serializable;

/*
 * This class allows to traffic between client and server information Robot by Serializable
 */
public class Robot implements Serializable {
	public static final String MESSAGE_ERROR_JSON = "{message:\"Internal Error: communicating with the robot was lost.\"}";
	private static final long serialVersionUID = 1L;
	private String cardinalPoint;
	private int pointX;
	private int pointY;
	
	public Robot(){
		super();
	}	
	
	public Robot(String cardinalPoint, int pointX, int pointY) {
		super();
		this.cardinalPoint = cardinalPoint;
		this.pointX = pointX;
		this.pointY = pointY;
	}
	
	public String getCardinalPoint() {
		return cardinalPoint;
	}
	public void setCardinalPoint(String cardinalPoint) {
		this.cardinalPoint = cardinalPoint;
	}
	
	public int getPointX() {
		return pointX;
	}
	public void setPointX(int pointX) {
		this.pointX = pointX;
	}
	public int getPointY() {
		return pointY;
	}
	public void setPointY(int pointY) {
		this.pointY = pointY;
	}
	
	
	

}
