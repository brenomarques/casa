package br.com.casa;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends Activity implements  View.OnClickListener{

    private static final String API_URL_COMMAND = "http://casadashboard.appspot.com/backend/apiV1?command=%s";
    private static final String CARDINAL_POINT = "cardinalPoint";
    private static final String POINT_X = "pointX";
    private static final String POINT_Y = "pointY";
    private static final String NORTH = "N";
    private static final String SOUTH = "S";
    private static final String EAST  = "E";
    private static final String WEST  = "W";
    private static final String ROBOT = "robot";



    private ImageButton imageButton_robot;
    private TextView textView_x;
    private TextView textView_y;
    private ImageButton imageButton_action_l;
    private ImageButton imageButton_action_r;
    private ImageButton imageButton_action_m;
    private ImageButton imageButton_action_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        imageButton_robot = (ImageButton) findViewById(R.id.imageButton_robot);
        textView_x = (TextView) findViewById(R.id.textView_x);
        textView_y = (TextView) findViewById(R.id.textView_y);
        imageButton_action_l = (ImageButton) findViewById(R.id.imageButton_action_l);
        imageButton_action_r = (ImageButton) findViewById(R.id.imageButton_action_r);
        imageButton_action_m = (ImageButton) findViewById(R.id.imageButton_action_m);
        imageButton_action_location = (ImageButton) findViewById(R.id.imageButton_action_location);
        imageButton_action_l.setOnClickListener(this);
        imageButton_action_r.setOnClickListener(this);
        imageButton_action_m.setOnClickListener(this);
        imageButton_action_location.setOnClickListener(this);

        textView_x.setVisibility(View.GONE);
        textView_y.setVisibility(View.GONE);
        imageButton_robot.setVisibility(View.GONE);

        if (!HelperNetwork.isConnected(this)) {
            HelperDialogs.showGeneralError(this, "You need an internet connection!");
        } else {
            new SendCommandTask(this, "").execute();
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if ( !HelperNetwork.isConnected(this)){
            HelperDialogs.showGeneralError(this,"You need an internet connection!");
        } else if ( view.equals(imageButton_action_l)) {
           new SendCommandTask(this, "L").execute();
        } else if ( view.equals(imageButton_action_r)) {
            new SendCommandTask(this, "R").execute();
        } else if ( view.equals(imageButton_action_m)) {
            new SendCommandTask(this, "M").execute();
        } else if ( view.equals(imageButton_action_location)) {
            new SendCommandTask(this, "").execute();
        }

    }

    public class SendCommandTask extends AsyncTask<Void, Void, JSONObject> {

        Context context;
        String command;
        Exception e;
        SendCommandTask(Context context,String command) {
            this.context = context;
            this.command = command;
            HelperDialogs.showProgressDialog(context);

        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            try {
                return HelperNetwork.getJson(String.format(API_URL_COMMAND,command));
            } catch (Exception e) {
                this.e = e;
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject background_return) {
            if ( e != null){
                HelperDialogs.showGeneralError(context,e.getMessage());
            }
            else {
                updateRobotStatus(background_return);
            }
            HelperDialogs.closeProgressDialog(context);

        }

        @Override
        protected void onCancelled() {
            HelperDialogs.closeProgressDialog(context);

        }

        private void updateRobotStatus(JSONObject jsonObject){
            try {
                if ( !jsonObject.isNull("error") ){
                    HelperDialogs.showGeneralError(context,jsonObject.getString("error"));
                }

                JSONObject jsonObjectRobot = jsonObject.getJSONObject(ROBOT);
                textView_x.setVisibility(View.GONE);
                textView_y.setVisibility(View.GONE);
                imageButton_robot.setVisibility(View.GONE);

                textView_x.setText(String.format("X : %s",jsonObjectRobot.getString(POINT_X)));
                textView_y.setText(String.format("Y : %s",jsonObjectRobot.getString(POINT_Y)));
                switch(jsonObjectRobot.getString(CARDINAL_POINT)){
                    case NORTH: imageButton_robot.setBackgroundResource(R.drawable.ic_robot_north);
                        break;
                    case SOUTH: imageButton_robot.setBackgroundResource(R.drawable.ic_robot_south);
                        break;
                    case WEST: imageButton_robot.setBackgroundResource(R.drawable.ic_robot_west);
                        break;
                    case EAST: imageButton_robot.setBackgroundResource(R.drawable.ic_robot_east);
                        break;
                    default: imageButton_robot.setVisibility(View.GONE);
                        break;

                }
                textView_x.setVisibility(View.VISIBLE);
                textView_y.setVisibility(View.VISIBLE);
                imageButton_robot.setVisibility(View.VISIBLE);


            } catch (JSONException e1) {
                e1.printStackTrace();
                imageButton_robot.setVisibility(View.GONE);
            }



        }
    }
}
