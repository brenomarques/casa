package br.com.casa;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by brenomar on 01/12/14.
 */
public class HelperDialogs {

    private static ProgressDialog progressDialog;

    private static ProgressDialog getProgressDialog(Context context){
        if ( progressDialog == null )
            progressDialog = new ProgressDialog(context);
        return progressDialog;
    }

    public static void showProgressDialog(Context context){
        getProgressDialog(context).show();
    }

    public static void closeProgressDialog(Context context){
        getProgressDialog(context).hide();
    }

    public static void showGeneralError(Context context, String error){
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }




}

